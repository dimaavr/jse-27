package ru.tsc.avramenko.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
        System.out.println("Created: " + task.getCreated());
        System.out.println("Start Date: " + task.getStartDate());
        System.out.println("Finish Date: " + task.getFinishDate());
    }

    protected Task add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return new Task(name, description);
    }

}